"""PyIDE git plugin."""
import git
import os
from threading import Timer

from gi.repository import GObject, Gtk, Pango


class Plugin(GObject.GObject):
    """The plugin class."""

    def __init__(self, *args, **kwargs):
        """Docstrings default."""
        GObject.GObject.__init__(self)
        self.has_pending = False
        self.working = False

        if 'application_window' in kwargs:
            self.application_window = kwargs['application_window']
            self.path = self.application_window.tree_view.path
            self.thread_manager = self.application_window.\
                get_application().thread_manager

        if 'settings' in kwargs:
            self.settings = kwargs['settings']

    def do_activate(self):
        """Docstrings default."""
        builder = Gtk.Builder.new_from_file(
            os.path.join(os.path.dirname(os.path.abspath(__file__)),
                         "res",
                         "layout",
                         "layout.ui"))

        go = builder.get_object
        self.layout = go('layout_stack')
        self.combo_box = go('branches_combo_box')
        self.combo_box.connect('changed', self._on_combo_box_changed)

        self.skip_dialog = go('no_repo_dialog')

        self.skip_git_button = go('skip_git_repo_button')
        self.skip_git_button.connect('clicked', self._skip)
        self.initialize_git_button = go('initialize_git_repo_button')
        self.initialize_git_button.connect('clicked', self._initialize)

        self.branch_store = Gtk.ListStore(str)
        self.combo_box.set_model(self.branch_store)
        self.combo_text_renderer = Gtk.CellRendererText()
        self.combo_box.pack_start(self.combo_text_renderer, True)
        self.combo_box.add_attribute(self.combo_text_renderer, "text", 0)

        self.diff_list_box = go('diff_list_box')
        self.diff_list_box_box = go('diff_list_box_box')

        self.commit_entry = go('commit_message_entry')
        self.commit_button = go('confirm_commit_button')
        self.commit_button.connect('clicked', self._commit)
        # self.commit_button.set_sensitive(False)

        self.staged_list_box = go('staged_list_box')
        self.staged_list_box_box = go('staged_list_box_box')

        self.no_changes_label = go('no_changes_label')

        self.application_window.side_bar_stack.add_titled(
            self.layout, "git_page", "Git")
        self.layout.show()

        self.check_dir()

    def check_dir(self):
        """Check if the current directory is a git repo."""
        try:
            self.repo = git.Repo(os.path.abspath(self.path),
                                 search_parent_directories=True)
            self.has_head = self.repo.git.rev_parse("--verify", "HEAD")
            print('HAS HEAD', self.has_head)

            self.application_window.tree_view.watcher_handler\
                .connect("changed", self.on_watcher_cb)

            self._set_branches()

            self.layout.set_visible_child_name('git_page')
            self.on_watcher_cb()
        except Exception as e:
            print(e)
            self.layout.set_visible_child_name('initialize_git_repo_page')

        # Ensure the layout will be shown
        self.layout.show()

    def _set_branches(self):
        """Set the current repo's branches and push to combo_box."""
        self.branches = [h.name for h in self.repo.heads]

        if len(self.branches) == 0:
            self.branches = ['master']

        self.branches_selected_index = 0
        self.active_branch = self.repo.active_branch

        self.branch_store.clear()

        for i, branch in enumerate(self.branches, start=0):
            self.branch_store.append([branch])
            if branch == self.active_branch.name:
                self.branches_selected_index = i

        self.combo_box.set_active(self.branches_selected_index)

    def _skip(self, *args):
        """Skip git repo initialization."""
        # TODO actually create the background watcher method
        self.skip_dialog.set_transient_for(self.application_window)
        self.skip_dialog.connect('response', self._skip_response)
        self.skip_dialog.run()
        self.layout.hide()

    def _skip_response(self, *args):
        self.skip_dialog.destroy()

    def _initialize(self, *args):
        try:
            self.repo = git.Repo.init(self.path, bare=False)
            print("Git repo initialized")
            self.check_dir()
        except Exception as e:
            print("Couldn't initialize repo", e)

    def _add(self, button, path):
        _path = str(path).replace(
            self.repo.git.rev_parse("--show-toplevel") + '/', '')
        print("Add %s" % _path)
        self.repo.git.add(path)
        self.on_watcher_cb()

    def _commit(self, *args):
        self.repo.git.commit("-m", self.commit_entry.get_text())
        self.commit_entry.set_text("")
        self.on_watcher_cb()

    def _remove(self, button, path):
        _path = str(path).replace(
            self.repo.git.rev_parse("--show-toplevel") + '/', '')
        print("Remove %s" % _path)
        self.repo.git.reset(_path)
        self.on_watcher_cb()

    def on_watcher_cb(self, *args):
        """."""
        if self.working:
            # If the plugin is working and a new not pending update
            # ocurred
            if self.has_pending and not ('is_pending' in args):
                print('Git skipped update too fast')
                return
            # If the update was pending
            else:
                Timer(2.0, self.on_watcher_cb, 'is_pending')
                return
        else:
            if self.has_pending and 'is_pending' in args:
                self.has_pending = False

        self.working = True
        self.thread_manager.make_thread(self._cb,
                                        self._get_unstaged,
                                        [],
                                        'git_uns_thread')
        self.thread_manager.make_thread(self._cb,
                                        self._get_staged,
                                        [],
                                        'git_stg_thread')

    def _cb(self, *args):
        # print('CB', args)
        staged = args[1][0]
        args[1].pop(0)
        list_box = self.diff_list_box
        list_box_box = self.diff_list_box_box

        if staged:
            list_box = self.staged_list_box
            list_box_box = self.staged_list_box_box

        for child in list_box.get_children():
            list_box.remove(child)

        rows = args[1]
        for row in rows:
            list_box.insert(row, -1)
            row.show_all()

        if len(rows) > 0:
            list_box_box.show()
            self.no_changes_label.hide()
        else:
            list_box_box.hide()
            self.no_changes_label.show()

        self.working = False

        print('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', self.repo.index.diff("HEAD"))

    def _get_unstaged(self, *args):
        lst = self._get_changed()
        lst.insert(0, False)
        return lst

    def _get_staged(self, *args):
        lst = self._get_changed(unstaged=False)
        lst.insert(0, True)
        return lst

    def _get_changed(self, unstaged=True):

        _diff = None
        icon_name = 'list-add-symbolic'
        fn = self._add
        if not unstaged:
            _diff = "HEAD"
            icon_name = 'list-remove-symbolic'
            fn = self._remove

        # Adding changes
        changes = [{'mod': diff.change_type, 'path': diff.a_blob.abspath}
                   for diff in self.repo.index.diff(_diff)]
        if unstaged:
            # Adding untracked files
            changes += [{'mod': 'U', 'path': os.path.abspath(un)}
                        for un in self.repo.untracked_files]

        rows = []
        # Creating the rows
        for change in changes:
            row = Gtk.ListBoxRow()
            box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            relative_path = change['path'].replace(
                self.settings.root + '/', '')

            lbl = Gtk.Label(os.path.basename(change['path']))
            lbl.get_style_context().add_class('font-small')
            lbl.set_ellipsize(Pango.EllipsizeMode.START)
            lbl.set_tooltip_text(relative_path)

            # TODO change the icon and the connection according to
            # staged or unstaged
            btn = Gtk.Button.new_from_icon_name(icon_name,
                                                Gtk.IconSize.MENU)
            # btn.set_relief(Gtk.ReliefStyle.NONE)
            btn.get_style_context().add_class('small-btn')
            btn.get_style_context().add_class('flat')
            btn.connect('clicked', fn, change['path'])

            mod = Gtk.Label(str(change['mod']).lower())
            # Applying font style
            mod.get_style_context().add_class(
                ('font-green' if change['mod'] == 'U'
                    else 'font-yellow' if change['mod'] == 'M'
                    else 'font-red')
            )

            box.pack_start(lbl, False, False, 5)
            box.pack_end(btn, False, False, 0)
            box.pack_end(mod, False, False, 6)

            row.add(box)
            rows.append(row)

        return rows  # Return the rows

    def _on_combo_box_changed(self, *args):
        print(self.combo_box.get_active_iter())  # #
